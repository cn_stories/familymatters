# The Spenglers: Ma�l-Str�m #

## A Real Ghostbusters Fan Fiction ##

Dr. Egon Spengler is still having trouble sleeping after his fall from the World Trade Center. Even after finally capturing the Boogieman and acknowledging his fear, his PTSD still leaves him sleepless. Worse, he starts dreaming of the return of another of his supernatural childhood nightmares. Egon calls on his cousin for help. But will his decision destroy himself, his family, and the ghostbusters in the process?